import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDict;
import crafttweaker.oredict.IOreDictEntry;
import mods.jei.JEI;

var darkMatterBlock = <projecte:matter_block:0>;
var darkMatter = <projecte:item.pe_matter:0>;
var redMatterBlock = <projecte:matter_block:1>;
var redMatter = <projecte:item.pe_matter:1>;
var alchFuel = <projecte:item.pe_fuel:0>;
var mobiFuel = <projecte:item.pe_fuel:1>;
var aeterFuel = <projecte:item.pe_fuel:2>;
var philStone = <projecte:item.pe_philosophers_stone>;
var coal = <ore:coal>;
var alchChest = <projecte:alchemical_chest>;
var condensemk1 = <projecte:condenser_mk1>;
var condensemk2 = <projecte:condenser_mk2>;
var transmuteTable = <projecte:transmutation_table>;
var transmuteTablet = <projecte:item.pe_transmutation_tablet>;
var darkMatterSword = <projecte:item.pe_dm_sword>;
var darkMatterPick = <projecte:item.pe_dm_pick>;
var darkMatterAxe = <projecte:item.pe_dm_axe>;
var darkMatterShovel = <projecte:item.pe_dm_shovel>;
var darkMatterHelmet = <projecte:item.pe_dm_armor_3>;
var darkMatterChestplate = <projecte:item.pe_dm_armor_2>;
var darkMatterLeggins = <projecte:item.pe_dm_armor_1>;
var darkMatterBoots = <projecte:item.pe_dm_armor_0>;
var redMatterSword = <projecte:item.pe_rm_sword>;
var redMatterPick = <projecte:item.pe_rm_pick>;
var redMatterAxe = <projecte:item.pe_rm_axe>;
var redMatterShovel = <projecte:item.pe_rm_shovel>;
var redMatterHelmet = <projecte:item.pe_rm_armor_3>;
var redMatterChestplate = <projecte:item.pe_rm_armor_2>;
var redMatterLeggins = <projecte:item.pe_rm_armor_1>;
var redMatterBoots = <projecte:item.pe_rm_armor_0>;
print("Initializing Harder ProjectE Recipes!");

//Dark Matter
//Removing Dark Matter Block Recipe.
recipes.remove(darkMatterBlock);
recipes.addShaped("Dark Matter Block", <projecte:matter_block>, 
[[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>]]);
//Removing Dark Matter Recipe
recipes.remove(darkMatter);
recipes.addShaped("Dark Matter", <projecte:item.pe_matter>, 
[[<projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>],
[<minecraft:diamond>, <minecraft:diamond_block>, <minecraft:diamond>],
[<projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>]]);
//Dark Matter

//Red Matter
//Removing Red Matter Block Recipe.
recipes.remove(redMatterBlock);
recipes.addShaped("Red Matter Block", <projecte:matter_block:1>*1, 
[[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>]]);
//Remove Red Matter Recipe.
recipes.remove(redMatter);
recipes.addShaped("Red Matter", <projecte:item.pe_matter:1>, 
[[<projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>],
[<minecraft:diamond>, <projecte:matter_block>, <minecraft:diamond>],
[<projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>, <projecte:item.pe_fuel:2>]]);
//Red Matter

//ProjectE Fuel
//Alchemical Fuel
recipes.remove(alchFuel);
recipes.addShaped("Alchemical Coal", <projecte:item.pe_fuel>*2, 
[[<minecraft:coal>, <minecraft:glowstone_dust>, <minecraft:coal>],
[<minecraft:glowstone_dust>, <projecte:item.pe_philosophers_stone>, <minecraft:glowstone_dust>],
[<minecraft:redstone>, <minecraft:diamond>, <minecraft:redstone>]]);
//Mobius Fuel
recipes.remove(mobiFuel);
recipes.addShaped("Mobius Fuel", <projecte:item.pe_fuel:1>*2, 
[[<minecraft:glowstone_dust>, <projecte:item.pe_fuel>, <minecraft:glowstone_dust>],
[<projecte:item.pe_fuel>, <projecte:item.pe_philosophers_stone>, <projecte:item.pe_fuel>],
[<minecraft:glowstone_dust>, <minecraft:diamond>, <minecraft:glowstone_dust>]]);
//Aeternalis Fuel
recipes.remove(aeterFuel);
recipes.addShaped("Aeternalis Fuel", <projecte:item.pe_fuel:2>*2, 
[[<minecraft:glowstone_dust>, <projecte:item.pe_fuel:1>, <minecraft:glowstone_dust>],
[<projecte:item.pe_fuel:1>, <projecte:item.pe_philosophers_stone>, <projecte:item.pe_fuel:1>],
[<projecte:item.pe_fuel>, <minecraft:diamond>, <projecte:item.pe_fuel>]]);
//ProjectE Fuel

//ProjectE Chests
recipes.remove(alchChest);
recipes.addShaped("Alchemical Chest", <projecte:alchemical_chest>, 
[[<projecte:item.pe_covalence_dust>, <projecte:item.pe_covalence_dust:1>, <projecte:item.pe_covalence_dust:2>],
[<minecraft:diamond>, <minecraft:chest>, <minecraft:diamond>],
[<projecte:item.pe_covalence_dust:2>, <projecte:item.pe_covalence_dust:1>, <projecte:item.pe_covalence_dust>]]);
recipes.remove(condensemk1);
recipes.addShaped("Energy Condenser", <projecte:condenser_mk1>, 
[[<minecraft:glowstone_dust>, <minecraft:diamond>, <minecraft:glowstone_dust>],
[<minecraft:diamond>, <projecte:alchemical_chest>, <minecraft:diamond>],
[<minecraft:obsidian>, <projecte:item.pe_matter>, <minecraft:obsidian>]]);
recipes.remove(condensemk2);
recipes.addShaped("Energy Condenser Mk2", <projecte:condenser_mk2>, 
[[<projecte:item.pe_fuel:2>, <minecraft:glowstone_dust>, <projecte:item.pe_fuel:2>],
[<minecraft:glowstone_dust>, <projecte:condenser_mk1>, <minecraft:glowstone_dust>],
[<minecraft:diamond>, <projecte:item.pe_matter>, <minecraft:diamond>]]);
//ProjectE Chests
//Transmute
recipes.remove(transmuteTable);
recipes.addShaped("Transmutation Table", <projecte:transmutation_table>, 
[[<minecraft:glowstone_dust>, <projecte:item.pe_matter>, <minecraft:glowstone_dust>],
[<projecte:item.pe_fuel:2>, <minecraft:diamond>, <projecte:item.pe_fuel:2>],
[<minecraft:obsidian>, <minecraft:obsidian>, <minecraft:obsidian>]]);
recipes.remove(transmuteTablet);
recipes.addShaped("Transmutation Tablet", <projecte:item.pe_transmutation_tablet>, 
[[<projecte:item.pe_matter>, <minecraft:glowstone_dust>, <projecte:item.pe_matter>],
[<projecte:item.pe_fuel:2>, <projecte:transmutation_table>, <projecte:item.pe_fuel:2>],
[<minecraft:diamond>, <projecte:item.pe_matter>, <minecraft:diamond>]]);
//Transmute

//Weapons
recipes.remove(darkMatterSword);
recipes.addShaped("Dark Matter Sword", <projecte:item.pe_dm_sword>, 
[[null, null, <projecte:item.pe_matter>],
[<minecraft:diamond>, <projecte:item.pe_matter>, null],
[<minecraft:diamond>, <minecraft:diamond>, null]]);
recipes.remove(darkMatterPick);
recipes.addShaped("Dark Matter Pickaxe", <projecte:item.pe_dm_pick>, 
[[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <minecraft:diamond>],
[null, <minecraft:diamond>, <projecte:item.pe_matter>],
[<minecraft:diamond>, null, <projecte:item.pe_matter>]]);
recipes.remove(darkMatterAxe);
recipes.addShaped("Dark Matter Axe", <projecte:item.pe_dm_axe>, 
[[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, <minecraft:diamond>, null],
[null, <minecraft:diamond>, null]]);
recipes.remove(darkMatterShovel);
recipes.addShaped("Dark Matter Shovel", <projecte:item.pe_dm_shovel>, 
[[null, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[null, <minecraft:diamond>, <projecte:item.pe_matter>],
[<minecraft:diamond>, null, null]]);
recipes.remove(redMatterSword);
recipes.addShaped("Red Matter Sword", <projecte:item.pe_rm_sword>, 
[[null, null, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter>, <projecte:item.pe_matter:1>, null],
[<projecte:item.pe_dm_sword>, <projecte:item.pe_matter>, null]]);
recipes.remove(redMatterPick);
recipes.addShaped("Red Matter Pickaxe", <projecte:item.pe_rm_pick>, 
[[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, null],
[null, <projecte:item.pe_dm_pick>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter>, null, <projecte:item.pe_matter:1>]]);
recipes.remove(redMatterAxe);
recipes.addShaped("Red Matter Axe", <projecte:item.pe_rm_axe>, 
[[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_dm_axe>, null],
[null, <projecte:item.pe_matter>, null]]);
recipes.remove(redMatterShovel);
recipes.addShaped("Red Matter Shovel", <projecte:item.pe_rm_shovel>, 
[[null, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>],
[null, <projecte:item.pe_dm_shovel>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter>, null, null]]);
//Weapons

//Armor
recipes.remove(darkMatterHelmet);
recipes.addShaped("Dark Matter Helmet", <projecte:item.pe_dm_armor_3>, 
[[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>]]);
recipes.remove(darkMatterChestplate);
recipes.addShaped("Dark Matter Chestplate", <projecte:item.pe_dm_armor_2>, 
[[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, <minecraft:diamond_block>, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>]]);
recipes.remove(darkMatterLeggins);
recipes.addShaped("Dark Matter Leggings", <projecte:item.pe_dm_armor_1>, 
[[<projecte:item.pe_matter>, <projecte:item.pe_matter>, <projecte:item.pe_matter>],
[<minecraft:diamond>, null, <minecraft:diamond>],
[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>]]);
recipes.remove(darkMatterBoots);
recipes.addShaped("Dark Matter Boots", <projecte:item.pe_dm_armor_0>, 
[[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>],
[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>],
[<minecraft:diamond>, null, <minecraft:diamond>]]);
recipes.remove(redMatterHelmet);
recipes.addShaped("Red Matter Helmet", <projecte:item.pe_rm_armor_3>, 
[[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_dm_armor_3>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>]]);
recipes.remove(darkMatterChestplate);
recipes.addShaped("Red Matter Chestplate", <projecte:item.pe_rm_armor_2>, 
[[<projecte:item.pe_matter:1>, <projecte:item.pe_matter>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_rm_armor_2>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>]]);
recipes.remove(redMatterLeggins);
recipes.addShaped("Red Matter Leggings", <projecte:item.pe_rm_armor_1>, 
[[<projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_dm_armor_1>, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_matter>, <projecte:item.pe_matter:1>]]);
recipes.remove(redMatterBoots);
recipes.addShaped("Red Matter Boots", <projecte:item.pe_rm_armor_0>, 
[[<projecte:item.pe_matter>, null, <projecte:item.pe_matter>],
[<projecte:item.pe_matter:1>, null, <projecte:item.pe_matter:1>],
[<projecte:item.pe_matter:1>, <projecte:item.pe_dm_armor_0>, <projecte:item.pe_matter:1>]]);
//Armor
